from trytond.pool import PoolMeta
from trytond.model import fields


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    operation_center = fields.Many2One('company.operation_center', 'Operation Center')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()


class Payroll(metaclass=PoolMeta):
    "Staff Payroll"
    __name__ = "staff.payroll"

    operation_center = fields.Many2One('company.operation_center', 'Operation Center')

    @classmethod
    def __setup__(cls):
        super(Payroll, cls).__setup__()

    @fields.depends('period', 'start', 'end', 'employee')
    def on_change_period(self):
        super(Payroll, self).on_change_period()
        if self.employee and self.employee.operation_center:
            self.operation_center = self.employee.operation_center


class PayrollGroup(metaclass=PoolMeta):
    'Payroll Group'

    __name__ = 'staff.payroll_group'

    def get_values(self, contract, start_date, end_date):
        values = super(PayrollGroup, self).get_values(contract, start_date, end_date)
        values['operation_center'] = contract.employee.operation_center if contract.employee.operation_center else None
        return values


class PayrollLine(metaclass=PoolMeta):
    "Payroll Line"
    __name__ = "staff.payroll.line"

    def get_move_line(self, account, party_id, amount):
        line = super(PayrollLine, self).get_move_line(account, party_id, amount)
        line.update({'operation_center': self.payroll.operation_center})
        return line
    

class Liquidation(metaclass=PoolMeta):
    __name__ = "staff.liquidation"
    
    operation_center = fields.Many2One('company.operation_center', 'Operation Center')

    def get_moves_lines(self):
        move_lines, grouped = super(Liquidation, self).get_moves_lines()
        if self.operation_center:
            for line in move_lines:
                line['operation_center'] = self.operation_center.id
        return move_lines, grouped