# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from collections import defaultdict
from itertools import tee, zip_longest
from operator import itemgetter
# from datetime import date as Date
# try:
#     import pygal
# except ImportError:
#     pygal = None
from dateutil.relativedelta import relativedelta
from sql import Null, Literal, Column, With
from sql.aggregate import Sum, Min, Count
from sql.conditionals import Coalesce
from sql.functions import CurrentTimestamp, DateTrunc
from trytond.modules.company import CompanyReport
from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView, UnionMixin, fields
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction
from trytond.pyson import Eval, If
from trytond.wizard import Wizard, StateTransition, StateAction
from trytond.i18n import lazy_gettext


def pairwise(iterable):
    a, b = tee(iterable)
    next(b)
    return zip_longest(a, b)


class Abstract(ModelSQL):

    company = fields.Many2One(
        'company.company', lazy_gettext("company_operation.msg_account_reporting_company"))
    number = fields.Integer(lazy_gettext("company_operation.msg_account_reporting_number"),
        help=lazy_gettext("company_operation.msg_account_reporting_number_help"))
    # account = fields.Many2One('account.account', lazy_gettext("company_operation.msg_account_reporting_account"))
    # operation_center = fields.Many2One('company.operation_center', lazy_gettext("company_operation.msg_account_reporting_operation_center"))
    debit = fields.Numeric(
        lazy_gettext("company_operation.msg_account_reporting_debit"),
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    credit = fields.Numeric(
        lazy_gettext("company_operation.msg_account_reporting_credit"),
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    balance = fields.Numeric(
        lazy_gettext("company_operation.msg_account_reporting_balance"),
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    # revenue_trend = fields.Function(
    #     fields.Char(lazy_gettext("company_operation.msg_account_reporting_revenue_trend")),
    #     'get_trend')
    time_series = None

    currency = fields.Function(fields.Many2One(
            'currency.currency',
            lazy_gettext("company_operation.msg_account_reporting_currency")),
        'get_currency')
    currency_digits = fields.Function(
        fields.Integer(
            lazy_gettext("company_operation.msg_account_reporting_currency_digits")),
        'get_currency_digits')

    @classmethod
    def table_query(cls):
        from_item, tables, withs = cls._joins()
        return from_item.select(*cls._columns(tables, withs),
            where=cls._where(tables, withs),
            group_by=cls._group_by(tables, withs),
            with_=withs.values())

    @classmethod
    def _joins(cls):
        pool = Pool()
        Company = pool.get('company.company')
        # Currency = pool.get('currency.currency')
        Line = pool.get('account.move.line')
        Move = pool.get('account.move')
        Account = pool.get('account.account')

        tables = {}
        tables['line'] = line = Line.__table__()
        tables['line.account'] = account = Account.__table__()
        tables['line.move'] = move = Move.__table__()
        tables['line.move.company'] = company = Company.__table__()
        withs = {}
        # currency_move = With(query=Currency.currency_rate_sql())
        # withs['currency_move'] = currency_move
        # currency_company = With(query=Currency.currency_rate_sql())
        # withs['currency_company'] = currency_company

        from_item = (line
            .join(move, condition=line.move == move.id)
            .join(company, condition=move.company == company.id)
            .join(account, condition=line.account == account.id))
        return from_item, tables, withs

    @classmethod
    def _columns(cls, tables, withs):
        line = tables['line']
        move = tables['line.move']

        # quantity = Coalesce(line.actual_quantity, line.quantity)
        # account = cls.account.sql_cast(line.account)
        debit = cls.debit.sql_cast(Sum(Coalesce(line.debit, 0)))
        credit = cls.credit.sql_cast(Sum(Coalesce(line.credit, 0)))
        balance = cls.balance.sql_cast(
            Sum(Coalesce(line.credit, 0) - Coalesce(line.debit, 0)))
        return [
            cls._column_id(tables, withs).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            cls.write_uid.sql_cast(Literal(Null)).as_('write_uid'),
            cls.write_date.sql_cast(Literal(Null)).as_('write_date'),
            move.company.as_('company'),
            # account.as_('account'),
            debit.as_('debit'),
            credit.as_('credit'),
            balance.as_('balance'),
            Count(move.id, distinct=True).as_('number'),
            ]

    @classmethod
    def _column_id(cls, tables, withs):
        line = tables['line']
        return Min(line.id)

    @classmethod
    def _group_by(cls, tables, withs):
        move = tables['line.move']
        # line = tables['line']
        # return [move.company, line.account]
        return [move.company]

    @classmethod
    def _where(cls, tables, withs):
        context = Transaction().context
        move = tables['line.move']
        line = tables['line']

        where = move.company == context.get('company')
        where &= move.state.in_(cls._move_states())
        from_date = context.get('from_date')
        if from_date:
            where &= move.date >= from_date
        to_date = context.get('to_date')
        if to_date:
            where &= move.date <= to_date
        where &= line.account.in_(cls._get_accounts())
        return where

    @classmethod
    def _get_accounts(cls):
        context = Transaction().context
        from_account, to_account = itemgetter('from_account', 'to_account')(context)
        Account = Pool().get('account.account')
        domain = []
        if from_account:
            from_account, = Account.browse([from_account])
            domain.append(('code', '>=', from_account.code))
        if to_account:
            to_account, = Account.browse([to_account])
            domain.append(('code', '<=', to_account.code))
        accounts = Account.search(domain)
        return [a.id for a in accounts]

    @classmethod
    def _move_states(cls):
        return ['posted']

    @property
    def time_series_all(self):
        delta = self._period_delta()
        for ts, next_ts in pairwise(self.time_series or []):
            yield ts
            if delta and next_ts:
                date = ts.date + delta
                while date < next_ts.date:
                    yield None
                    date += delta

    @classmethod
    def _period_delta(cls):
        context = Transaction().context
        return {
            'year': relativedelta(years=1),
            'month': relativedelta(months=1),
            'day': relativedelta(days=1),
            }.get(context.get('period'))

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.company.currency.digits

    # def get_trend(self, name):
    #     name = name[:-len('_trend')]
    #     if pygal:
    #         chart = pygal.Line()
    #         chart.add('', [getattr(ts, name) if ts else 0
    #                 for ts in self.time_series_all])
    #         return chart.render_sparktext()


class AbstractTimeseries(Abstract):

    date = fields.Date("Date")

    @classmethod
    def __setup__(cls):
        super(AbstractTimeseries, cls).__setup__()
        cls._order = [('date', 'ASC')]

    @classmethod
    def _columns(cls, tables, withs):
        return super(AbstractTimeseries, cls)._columns(tables, withs) + [
            cls._column_date(tables, withs).as_('date')]

    @classmethod
    def _column_date(cls, tables, withs):
        context = Transaction().context
        move = tables['line.move']
        date = DateTrunc(context.get('period'), move.date)
        date = cls.date.sql_cast(date)
        return date

    @classmethod
    def _group_by(cls, tables, withs):
        return super(AbstractTimeseries, cls)._group_by(tables, withs) + [
            cls._column_date(tables, withs)]


class Context(ModelView):
    "Account Reporting Context"
    __name__ = 'account.reporting.context'

    company = fields.Many2One('company.company', "Company", required=True)
    from_date = fields.Date("From Date",
        domain=[
            If(Eval('to_date') & Eval('from_date'),
                ('from_date', '<=', Eval('to_date')),
                ()),
            ],
        depends=['to_date'])
    to_date = fields.Date("To Date",
        domain=[
            If(Eval('from_date') & Eval('to_date'),
                ('to_date', '>=', Eval('from_date')),
                ()),
            ],
        depends=['from_date'])
    period = fields.Selection([
            ('year', "Year"),
            ('month', "Month"),
            ('day', "Day"),
            ], "Period", required=True)
    operation_center = fields.Many2One(
        'company.operation_center', "Operation Center")

    from_account = fields.Many2One('account.account', "From Account",
        domain=[
            ('company', '=', Eval('company', -1)),
            ('type', '!=', None),
            ('code', '!=', None),
            ],
        depends=['company'])

    to_account = fields.Many2One('account.account', "To Account",
        domain=[
            ('company', '=', Eval('company', -1)),
            ('type', '!=', None),
            ('code', '!=', None),
            ],
        depends=['company'])

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_from_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'from_date' in context:
            return context['from_date']
        return Date.today() - relativedelta(years=1)

    @classmethod
    def default_to_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'to_date' in context:
            return context['to_date']
        return Date.today()

    @classmethod
    def default_period(cls):
        return Transaction().context.get('period', 'month')

    @classmethod
    def default_operation_center(cls):
        context = Transaction().context
        if 'operation_center' in context:
            return context['operation_center']

    @classmethod
    def default_from_account(cls):
        return Transaction().context.get('from_account')

    @classmethod
    def default_to_account(cls):
        return Transaction().context.get('to_account')


class OperationCenterMixin(object):
    __slots__ = ()
    operation_center = fields.Many2One(
        'company.operation_center', "Operation Center",
        context={
            'company': Eval('company', -1),
            },
        depends=['company'])

    @classmethod
    def _columns(cls, tables, withs):
        line = tables['line']
        operation_center = line.operation_center
        return super(OperationCenterMixin, cls)._columns(tables, withs) + [
            operation_center.as_('operation_center')]

    @classmethod
    def _group_by(cls, tables, withs):
        line = tables['line']
        return super(OperationCenterMixin, cls)._group_by(tables, withs) + [
            line.operation_center]

    @classmethod
    def _where(cls, tables, withs):
        where = super(OperationCenterMixin, cls)._where(tables, withs)
        context = Transaction().context
        line = tables['line']
        operation_center = context.get('operation_center')

        if operation_center:
            where &= line.operation_center == operation_center

        where &= line.operation_center != Null
        return where

    def get_rec_name(self, name):
        return self.operation_center.rec_name


class OperationCenter(OperationCenterMixin, Abstract, ModelView):
    "Account Reporting per Operation Center"
    __name__ = 'account.reporting.operation_center'

    time_series = fields.One2Many(
        'account.reporting.operation_center.time_series', 'Operation Center', "Time Series")

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('operation_center', 'ASC'))

    @classmethod
    def _column_id(cls, tables, withs):
        line = tables['line']
        return line.operation_center


class OperationCenterReport(CompanyReport):
    'Operation Center Report'
    __name__ = 'operation.center.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        from_account, to_account = itemgetter('from_account', 'to_account')(context)
        Account = Pool().get('account.account')
        report_context['from_account'] = None
        report_context['to_account'] = None
        if from_account is not None:
            account, = Account.browse([from_account])
            report_context['from_account'] = account.code
        if to_account is not None:
            account, = Account.browse([to_account])
            report_context['to_account'] = account.code
        return report_context


class OperationCenterTimeseries(OperationCenterMixin, AbstractTimeseries, ModelView):
    "Account Reporting per Operation Center"
    __name__ = 'account.reporting.operation_center.time_series'

# class CategoryMixin(object):

class OperationCenterDetailedReport(CompanyReport):
    'Operation Center Detailed Report'
    __name__ = 'operation.center.detailed.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        print(report_context,'asdas2222')
        from_account, to_account = itemgetter('from_account', 'to_account')(context)
        Account = Pool().get('account.account')
        MoveLine = Pool().get('account.move.line')
        domain = [
            ('move.date','>=',context['from_date']),
            ('move.date','<=',context['to_date']),
            ('operation_center','=',context['operation_center']),
        ]
        domain_account =[]
        print(from_account,to_account,'333333333333333')
        if from_account:
            from_account, = Account.browse([from_account])
            domain_account.append(('code', '>=', from_account.code))
        if to_account:
            to_account, = Account.browse([to_account])
            domain_account.append(('code', '<=', to_account.code))
        accounts = Account.search(domain_account)
        # print(from_account, '2222222222222222')
        lines = MoveLine.search(domain)
        report_context['records']= lines
        # report_context['from_account'] = from_account.code
        # report_context['to_account'] = to_account.code
        # return report_context
        return report_context,[a.id for a in accounts]
        
        
        