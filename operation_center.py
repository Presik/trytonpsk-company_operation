# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.tools import lstrip_wildcard


class OperationCenter(ModelSQL, ModelView):
    'Operation Center'
    __name__ = 'company.operation_center'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    company = fields.Many2One('company.company', 'Company', required=True)
    users = fields.Many2Many('res.user-company.operation_center',
        'operation_center', 'user', 'Users')
    electronic_authorization = fields.Many2One('account.invoice.authorization',
        'Electronic Authorization', domain=[
            ('kind', 'in', ['1', '2', '3', 'P']),
            ('company', '=', Eval('company')),
        ])
    electronic_authorization_credit = fields.Many2One(
        'account.invoice.authorization',
        'Credit Electronic Authorization', domain=[
            ('kind', 'in', ['1', '2', '3', 'P']),
            ('company', '=', Eval('company')),
        ])

    @classmethod
    def __setup__(cls):
        super(OperationCenter, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    def get_rec_name(self, name):
        if self.code:
            return self.code + ' - ' + self.name
        else:
            return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        code_value = clause[2]
        if clause[1].endswith('like'):
            code_value = lstrip_wildcard(clause[2])
        return [bool_op,
            ('code', clause[1], code_value) + tuple(clause[3:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]


class OperationCenterUser(ModelSQL):
    'OperationCenter User'
    __name__ = 'res.user-company.operation_center'
    _table = 'res_user_company_operation_center'
    operation_center = fields.Many2One('company.operation_center', 'OperationCenter',
        ondelete='CASCADE', select=True,  required=True)
    user = fields.Many2One('res.user', 'User', select=True,
        required=True, ondelete='RESTRICT')
