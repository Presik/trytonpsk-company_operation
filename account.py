# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from timeit import default_timer as timer
from datetime import timedelta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext

ZERO = Decimal(0)


class AuxiliaryByOCStart(ModelView):
    'Auxiliary By OC Start'
    __name__ = 'company_operation.auxiliary_by_oc.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    start_code = fields.Char('Start Code Account')
    end_code = fields.Char('End Code Account')
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')
    op_center = fields.Many2Many('company.operation_center', None, None,
        'Operation Center')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AuxiliaryByOC(Wizard):
    'Auxiliary By OC'
    __name__ = 'company_operation.auxiliary_by_oc'
    start = StateView('company_operation.auxiliary_by_oc.start',
        'company_operation.auxiliary_by_oc_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ]
    )
    print_ = StateReport('company_operation.auxiliary_by_oc')

    def do_print_(self, action):

        op_centers = [op.id for op in self.start.op_center]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'start_code': self.start.start_code,
            'end_code': self.start.end_code,
            'detailed': self.start.detailed,
            'fiscalyear': self.start.fiscalyear.name,
            'op_centers': op_centers,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AuxiliaryByOCReport(Report):
    __name__ = 'company_operation.auxiliary_by_oc'

    @classmethod
    def get_accs_balances(self, accounts_ids, end_date, start_date=None):
        cursor = Transaction().connection.cursor()
        if not start_date:
            start_date = '2000-01-01'
        else:
            start_date = str(start_date)

        if len(accounts_ids) == 1:
            accounts_id = accounts_ids[0]
            accounts_ids = f"({accounts_id})"
        else:
            accounts_ids = str(tuple(accounts_ids))
        end_date = str(end_date)
        query = f"""
            SELECT
                ml.account,
                sum(ml.debit),
                sum(ml.credit),
                (sum(ml.debit) - sum(ml.credit))
              FROM account_move_line AS ml
              JOIN account_move AS mv ON mv.id = ml.move
              WHERE
                mv.date>='{start_date}' AND mv.date<='{end_date}' AND
                ml.account IN {accounts_ids}
              GROUP BY account
        """
        cursor.execute(query)
        accs_balances = {}
        for acc_id, debit, credit, balance in list(cursor):
            accs_balances[acc_id] = {
                'debit': debit,
                'credit': credit,
                'balance': balance,
            }
        return accs_balances

    @classmethod
    def get_oc_balances(cls, accounts_ids, op_centers, end_date, start_date):
        cursor = Transaction().connection.cursor()
        end_date = str(end_date)
        if len(accounts_ids) == 1:
            accounts_id = accounts_ids[0]
            accounts_ids = f"({accounts_id})"
        else:
            accounts_ids = str(tuple(accounts_ids))

        ocs_cond = ''
        if op_centers:
            if len(op_centers) == 1:
                oc_id = op_centers[0]
                ocs = f"({oc_id})"
            else:
                ocs = str(tuple(op_centers))
            ocs_cond = f' AND operation_center IN {ocs}'

        query = f"""
            SELECT
                ml.account,
                ml.operation_center,
                sum(ml.debit),
                sum(ml.credit),
                (sum(ml.debit) - sum(ml.credit))
              FROM account_move_line AS ml
              JOIN account_move AS mv ON mv.id = ml.move
              WHERE
                mv.date>='{start_date}' AND mv.date<='{end_date}' AND
                ml.account IN {accounts_ids} {ocs_cond}
              GROUP BY ml.account, ml.operation_center
        """
        cursor.execute(query)
        oc_balances = {}
        for acc_id, oc_id, debit, credit, balance in list(cursor):
            oc_balances[(acc_id, oc_id)] = {
                'debit': debit,
                'credit': credit,
                'balance': balance,
            }
        return oc_balances

    @classmethod
    def get_target_accounts(cls, data):
        cursor = Transaction().connection.cursor()
        start_date = str(data['start_date'])
        end_date = str(data['end_date'])
        codes = ''
        if data['start_code']:
            start_ = data['start_code']
            codes = f" AND code>='{start_}'"
        if data['end_code']:
            end_ = data['end_code']
            codes += f" AND code<='{end_}'"

        query = f"""
            SELECT account, acc.code, acc.name FROM account_move_line AS ml
                JOIN account_move AS mv ON mv.id = ml.move
                JOIN account_account AS acc ON acc.id = ml.account
                    AND mv.date>='{start_date}' AND mv.date<='{end_date}' {codes}
                GROUP BY account, acc.code, acc.name ORDER BY acc.code
            """
        cursor.execute(query)
        accounts = {acc_id:
            {'code': code, 'name': name} for acc_id, code, name in list(cursor)
        }
        return accounts

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(data['company'])

        # --------------------------------------------------------------
        # Select only accounts moved in periods, no more
        accounts = cls.get_target_accounts(data)
        accounts_ids = accounts.keys()

        previous_date = data['start_date'] - timedelta(days=1)
        accs_balance = cls.get_accs_balances(accounts_ids, previous_date)
        oc_balances = cls.get_oc_balances(
            accounts_ids,
            data['op_centers'],
            data['end_date'],
            data['start_date']
        )

        records = cls.get_lines(accounts, accs_balance, oc_balances, data)

        report_context['records'] = records.values()
        report_context['company'] = company
        report_context['detailed'] = data['detailed']
        return report_context

    @classmethod
    def get_lines(cls, accounts, accs_balance, oc_balances, data):
        MoveLine = Pool().get('account.move.line')
        OC = Pool().get('company.operation_center')
        clause = [
            ('move.date', '>=', data['start_date']),
            ('move.date', '<=', data['end_date']),
        ]

        operation_centers = OC.browse(data['op_centers'])
        operation_centers = {op.id: {
            'name': op.name, 'code': op.code} for op in operation_centers
        }
        acc_matrix = {}
        line_read = MoveLine.search_read
        for acc_id, acc in accounts.items():
            acc_values = accs_balance.get(acc_id, None)
            if not acc_values:
                acc_values = {'debit': 0, 'credit': 0, 'balance': 0}
            acc_matrix[acc_id] = {
                'id': acc_id,
                'code': acc['code'],
                'name': acc['name'],
                'debit': acc_values['debit'],
                'credit': acc_values['credit'],
                'balance': acc_values['balance'],
                'total_debit': [acc_values['debit']],
                'total_credit': [acc_values['credit']],
                'total_balance': [acc_values['balance']],
                'ocs': [],
            }
            for oc_id in data['op_centers']:
                lines = []
                if (acc_id, oc_id) not in oc_balances.keys():
                    continue
                if data['detailed']:
                    _clause = copy.deepcopy(clause)
                    _clause.extend([
                        ('operation_center', '=', oc_id),
                        ('account', '=', acc_id),
                    ])
                    lines = line_read(
                        _clause,
                        order=[('date', 'ASC')],
                        fields_names=[
                            'description', 'move.number', 'move.date',
                            'party.name', 'party.id_number', 'account', 'debit',
                            'credit', 'reference', 'move_origin.rec_name',
                            'operation_center.code', 'operation_center.name'
                        ]
                    )
                    if not lines:
                        continue

                oc_lines = {
                    'lines': lines,
                    'name': operation_centers[oc_id]['name'],
                }
                start_balances = oc_balances[(acc_id, oc_id)]
                acc_matrix[acc_id]['total_debit'].append(start_balances['debit'])
                acc_matrix[acc_id]['total_credit'].append(start_balances['credit'])
                acc_matrix[acc_id]['total_balance'].append(start_balances['balance'])
                oc_lines.update(start_balances)
                acc_matrix[acc_id]['ocs'].append(oc_lines)
        return acc_matrix

#
# class AuxiliaryByOCReport(Report):
#     __name__ = 'company_operation.auxiliary_by_oc'
#
#     @classmethod
#     def get_target_accounts(cls, data):
#         cursor = Transaction().connection.cursor()
#         start_date = str(data['start_date'])
#         end_date = str(data['end_date'])
#         codes = ''
#         if data['start_code']:
#             start_ = data['start_code']
#             codes = f" AND code>='{start_}'"
#         if data['end_code']:
#             end_ = data['end_code']
#             codes += f" AND code<='{end_}'"
#
#         query = f"""
#             SELECT account, acc.code, acc.name FROM account_move_line AS ml
#                 JOIN account_move AS mv ON mv.id = ml.move
#                 JOIN account_account AS acc ON acc.id = ml.account
#                     AND mv.date>='{start_date}' AND mv.date<='{end_date}' {codes}
#                 GROUP BY account, acc.code, acc.name ORDER BY acc.code
#             """
#         cursor.execute(query)
#         accounts = {acc_id:
#             {'code': code, 'name': name} for acc_id, code, name in list(cursor)
#         }
#         return accounts
#
#     @classmethod
#     def get_parent_accounts(cls, accounts_ids):
#         cursor = Transaction().connection.cursor()
#         if accounts_ids:
#             accounts_ids = tuple(accounts_ids)
#         query = f"""WITH RECURSIVE t AS (
#                 SELECT id, ARRAY[]::integer[] AS parentlist
#                 FROM account_account WHERE parent IS NULL
#                 UNION ALL
#                 SELECT account_account.id, t.parentlist || account_account.parent
#                 FROM account_account, t
#                 WHERE account_account.parent = t.id
#                 ) SELECT * FROM t WHERE id in {accounts_ids};"""
#         cursor.execute(query)
#         account_parents = {acc_id: parentlist for acc_id, parentlist in list(cursor)}
#
#         return account_parents
#
#     @classmethod
#     def get_balances(cls, accounts_ids, data):
#         cursor = Transaction().connection.cursor()
#         cond1 = ''
#         if data['start_date']:
#             cond1 += f"and mv.date>='{data['start_date']}'"
#         if data['end_date']:
#             cond1 += f" and mv.date<='{data['end_date']}'"
#
#         if data['op_centers']:
#             op_ids = str(tuple(data['op_centers'])).replace(',', '') if len(
#                 data['op_centers']) == 1 else str(tuple(data['op_centers']))
#             cond1 += f" and ml.operation_center in {op_ids}"
#
#         query = f"""select  account, acc.code, acc.name, acc.left,
#                 concat(op.code, ' - ', op.name) AS op_center,
#                 sum(debit) as debit, sum(credit) as credit, (sum(debit)-sum(credit)) as balance
#                 from account_move_line as ml
#                 LEFT JOIN account_move AS mv ON mv.id = ml.move
#                 LEFT JOIN account_account as acc on ml.account = acc.id
#                 LEFT JOIN company_operation_center as op on ml.operation_center = op.id
#                 where account in {tuple(accounts_ids)} {cond1}
#                 group by grouping sets ((account, acc.code , acc.name), ( op_center, account, acc.code , acc.name, acc.left))
#                 order by acc.left, op_center ;"""
#         cursor.execute(query)
#         balances = [{'account': account, 'code': code,
#             'name': name, 'left': left, 'op_center': op_center, 'debit': debit,
#             'credit': credit, 'balance': balance} for account, code, name, left, op_center, debit, credit, balance in list(cursor)]
#
#         return balances
#
#     @classmethod
#     def get_order_accounts(cls, parent_accounts):
#         accounts_ids = set()
#         for p in parent_accounts.values():
#             accounts_ids.update(p)
#
#         accounts_ids.update(parent_accounts.keys())
#
#         cursor = Transaction().connection.cursor()
#         query = f'select id, code, name, parent, "left" from account_account where id in {tuple(accounts_ids)} order by "left";'
#         cursor.execute(query)
#
#         order_accounts = {id: {'code': code, 'name': name, 'parent': parent,
#             'left': left, 'debit': [], 'credit': [], 'balance': [], 'op_center': []}
#             for id, code, name, parent, left in list(cursor)}
#         return order_accounts
#
#     @classmethod
#     def get_context(cls, records, header, data):
#         start = timer()
#         report_context = super().get_context(records, header, data)
#
#         pool = Pool()
#         Company = pool.get('company.company')
#         company = Company(data['company'])
#
#         # --------------------------------------------------------------
#         # Select only accounts moved in periods, no more
#         accounts = cls.get_target_accounts(data)
#         accounts_ids = accounts.keys()
#         if not accounts_ids:
#             raise UserError(gettext('company_operation.msg_no_records'))
#         # print(accounts_ids, 'accounts_ids')
#         parent_accounts = cls.get_parent_accounts(accounts_ids)
#         order_accounts = cls.get_order_accounts(parent_accounts)
#         balances = cls.get_balances(accounts_ids, data)
#
#         end1 = timer()
#         delta1 = (end1 - start)
#         print('Delta 1.... ', delta1)
#         detailed = False
#         if data['detailed']:
#             detailed = cls.get_detailed(data, accounts_ids)
#         records = cls.get_records(order_accounts, balances, accounts_ids, parent_accounts, data, detailed)
#
#         report_context['records'] = records.values()
#         report_context['company'] = company
#         report_context['detailed'] = data['detailed']
#         end = timer()
#         delta_total = (end - start)
#         print('tiempo total --- :', delta_total)
#         return report_context
#
#     @classmethod
#     def get_detailed(cls, data, accounts_ids):
#         MoveLine = Pool().get('account.move.line')
#         dom = [
#             ('account', 'in', accounts_ids)
#         ]
#         if data['start_date']:
#             dom.append(('move.date', '>=', data['start_date']))
#         if data['end_date']:
#             dom.append(('move.date', '<=', data['end_date']))
#
#         if data['op_centers']:
#             dom.append(('operation_center', 'in', data['op_centers']))
#
#         fields_names = [
#             'description', 'move.number', 'move.date',
#             'party.name', 'party.id_number', 'account', 'debit',
#             'credit', 'reference', 'move_origin.rec_name',
#             'operation_center.rec_name', 'analytic_lines.rec_name'
#         ]
#         start = timer()
#         lines = MoveLine.search_read(dom, fields_names=fields_names)
#         end1 = timer()
#         delta1 = (end1 - start)
#         print('Delta 1.... ', delta1)
#         lines_dic = {}
#         for line in lines:
#             op = (line['operation_center.']['rec_name'] if line['operation_center.'] else ' - ')
#             ac = line['account']
#             key = str(ac)+' - '+op
#             try:
#                 lines_dic[key].append(line)
#             except:
#                 lines_dic[key] = [line]
#         return lines_dic
#
#     @classmethod
#     def get_records(cls, order_accounts, balances, accounts_ids, parent_accounts, data, detailed):
#
#         for b in balances:
#             if not b['op_center']:
#                 to_update = parent_accounts[b['account']]
#                 for ac in to_update:
#                     if ac != 1:
#                         order_accounts[ac]['balance'].append(b['balance'])
#                         order_accounts[ac]['debit'].append(b['debit'])
#                         order_accounts[ac]['credit'].append(b['credit'])
#                 order_accounts[b['account']]['balance'].append(b['balance'])
#                 order_accounts[b['account']]['debit'].append(b['debit'])
#                 order_accounts[b['account']]['credit'].append(b['credit'])
#
#             elif b['op_center']:
#                 try:
#                     order_accounts[b['account']]['op_center']['total'].append(b)
#                 except:
#                     order_accounts[b['account']]['op_center'] = {'total': [b]}
#                 if detailed:
#                     key = str(b['account']) + ' - ' + b['op_center']
#                     lines = detailed[key]
#                     print(key, b['account'], b['op_center'])
#                     order_accounts[b['account']]['op_center']['lines'] = lines
#
#         return order_accounts
