# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import date

from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.transaction import Transaction

_ZERO = Decimal('0.0')
TODAY = date.today()


class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center')

    @classmethod
    def _get_writeoff_move(cls, reconcile_account, reconcile_party, amount,
            date=None, writeoff=None, description=None):
        move = super(Line, cls)._get_writeoff_move(reconcile_account, reconcile_party, amount,
            date, writeoff, description)
        AnalyticLine = Pool().get('analytic_account.line')
        Move = Pool().get('account.move')
        context = Transaction().context
        if context.get('operation_center'):
            for line in move.lines:
                line.operation_center = context.get('operation_center')
                if line.account.type.statement == 'income':
                    account = context.get('analytic_account')
                    analytic = AnalyticLine()
                    analytic.account  = account
                    analytic.debit = line.debit
                    analytic.credit = line.credit
                    line.analytic_lines = [analytic]
        move.save()
        Move.post([move])
        return move


class ReconcileLinesWriteOff(metaclass=PoolMeta):
    __name__ = 'account.move.reconcile_lines.writeoff'

    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', required=True)

    analytic_account = fields.Many2One('analytic_account.account', 'Analytic Account', required=True)


class ReconcileLines(metaclass=PoolMeta):
    __name__ = 'account.move.reconcile_lines'

    def transition_reconcile(self):
        context = Transaction().context
        context['operation_center'] = self.writeoff.operation_center.id if hasattr(self.writeoff, 'operation_center') else None
        account = self.writeoff.analytic_account.id if hasattr(self.writeoff, 'analytic_account') else None
        context['analytic_account'] =  account
        with Transaction().set_context(context):
            super(ReconcileLines, self).transition_reconcile()
        return 'end'