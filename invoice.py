# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, And
from trytond.report import Report
from trytond.wizard import Wizard, StateView, StateReport, Button
from trytond.transaction import Transaction


STATES = {
    'readonly': Eval('state') != 'draft',
}

_ZERO = Decimal('0.00')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    _states = {'readonly': Eval('state') != 'draft'}
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states=_states)
    contract = fields.Char('Contract', states=_states)
    period = fields.Char('Period', states=_states)
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', states={
            'invisible': Eval('type') != 'in',
            'readonly': Eval('state') != 'draft'
        })

    def _get_move_line(self, date, amount):
        line = super(Invoice, self)._get_move_line(date, amount)
        if self.operation_center:
            line.operation_center = self.operation_center.id
        line.reference = self.reference
        return line

    def _credit(self, **values):
        credit = super(Invoice, self)._credit(**values)
        if self.operation_center:
            credit.operation_center = self.operation_center.id
            if credit.type != 'in':
                credit.invoice_type = '91'
        return credit


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    _states = {
        'readonly': Eval('invoice_state') != 'draft',
        'required': True,
    }
    _depends = ['invoice_state']
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states=_states, depends=_depends)

    def _credit(self):
        line = super(InvoiceLine, self)._credit()
        line.operation_center = self.operation_center
        return line

    def get_move_lines(self):
        lines = super(InvoiceLine, self).get_move_lines()
        for line in lines:
            if self.operation_center:
                line.operation_center = self.operation_center.id
            line.reference = self.invoice.reference
        return lines


class PaymentsByOCStart(ModelView):
    'Payments By Operation Center'
    __name__ = 'company_operation.payments_by_oc.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    detailed = fields.Boolean('Detailed')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PaymentsByOC(Wizard):
    'Payments By Operation Center'
    __name__ = 'company_operation.payments_by_oc'
    start = StateView('company_operation.payments_by_oc.start',
        'company_operation.payments_by_oc_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('company_operation.payments_by_oc_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class PaymentsByOCReport(Report):
    'Payments By Operation Center Report'
    __name__ = 'company_operation.payments_by_oc_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Order = pool.get('laboratory.order')
        Company = pool.get('company.company')
        company = Company(data['company'])

        dom = [
            ('company', '=', company),
            ('order_date', '>=', data['start_date']),
            ('order_date', '<=', data['end_date']),
        ]

        _records = {}
        orders = Order.search(dom)
        payment_total = []
        payment_total_account = []
        for order in orders:
            oc_id = order.operation_center.id
            if oc_id not in _records.keys():
                _records[oc_id] = {
                    'oc': order.operation_center.name,
                    'payments': {},
                    'total': [],
                    'total_account': [],
                }
            _payments = _records[oc_id]['payments']
            for payment in order.payments:
                amount = payment.amount
                amount_account = payment.amount_account
                _records[oc_id]['total'].append(amount)
                _records[oc_id]['total_account'].append(amount_account)
                payment_total.append(amount)
                payment_total_account.append(amount_account)
                payment_mode = payment.payment_mode
                if payment_mode.id not in _payments.keys():
                    _payments[payment_mode.id] = {
                        'name': payment_mode.name,
                        'total': [amount],
                        'total_account': [amount_account],
                    }
                else:
                    _payments[payment_mode.id]['total'].append(amount)
                    _payments[payment_mode.id]['total_account'].append(amount_account)

        report_context['records'] = _records.values()
        report_context['company'] = company
        report_context['payment_total'] = sum(payment_total)
        report_context['payment_total_account'] = sum(payment_total_account)
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        return report_context


class InvoicesStart(metaclass=PoolMeta):
    __name__ = 'invoice_report.print_invoices.start'
    op_center = fields.Many2Many('company.operation_center', None, None,
        'Operation Center')


class Invoices(metaclass=PoolMeta):
    __name__ = 'invoice_report.print_invoices'

    def do_print_(self, action):
        action, data = super(Invoices, self).do_print_(action)
        op_center_ids = [op.id for op in self.start.op_center]
        data['op_center'] = op_center_ids
        return action, data


class InvoicesReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.invoices_report'

    @classmethod
    def get_record(cls, invoice):
        rec = super(InvoicesReport, cls).get_record(invoice)
        rec['operation_center'] = invoice.operation_center and invoice.operation_center.name
        return rec

    @classmethod
    def get_domain_invoices(cls, data):
        dom_invoices = super(InvoicesReport, cls).get_domain_invoices(data)
        if data['op_center']:
            dom_invoices.append(
                ('operation_center', 'in', data['op_center']),
            )
        return dom_invoices
