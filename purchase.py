# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from functools import partial
from itertools import groupby
from trytond.report import Report
from trytond.i18n import gettext
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.modules.product import round_price
from trytond.tools import sortable_values
from trytond.transaction import Transaction
from trytond.modules.purchase.exceptions import PurchaseQuotationError
STATES = {
    'readonly': Eval('state') != 'draft',
}


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'readonly': Eval('state').in_(['confirmed', 'processed']),
        })

    @fields.depends('lines', 'operation_center')
    def on_change_operation_center(self):
        if self.operation_center:
            for ln in self.lines:
                ln.operation_center = self.operation_center.id

    def check_for_quotation(self):
        super(Purchase, self).check_for_quotation()
        for line in self.lines:
            if not line.operation_center:
                raise PurchaseQuotationError(
                    gettext(
                        'company_operation.msg_operation_center_required_for_quotation',
                        product=line.product.name
                    ))

    def _get_invoice_purchase(self):
        'Return invoice'
        invoice = super(Purchase, self)._get_invoice_purchase()
        context = Transaction().context
        if context.get('operation_center'):
            invoice.operation_center = context['operation_center']
        if context.get('reference'):
            invoice.reference = context['reference']
        if context.get('analytic_account'):
            invoice.analytic_account = context['analytic_account']

        return invoice

class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'readonly': Eval('purchase_state') != 'draft',
            }, depends=['purchase_state'])
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ])

    @fields.depends('analytic_accounts', 'analytic_account')
    def on_change_analytic_account(self):
        if self.analytic_account:
            for ac in self.analytic_accounts:
                ac.account = self.analytic_account.id
        else:
            for ac in self.analytic_accounts:
                ac.account= None

    def get_invoice_line(self):
        'Return a list of invoice line for purchase line'
        invoice_lines = super(PurchaseLine, self).get_invoice_line()
        if invoice_lines:
            invoice_line = invoice_lines[0]
            invoice_line.analytic_account = self.analytic_account
            invoice_line.operation_center = self.operation_center

        return invoice_lines


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'
    _states = {
        'readonly': Eval('state') != 'draft',
    }
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states=_states, required=True)
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ], states=_states, required=True)


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'
    _states = {
        'readonly': Eval('purchase_requisition_state') != 'draft',
    }

    # def compute_request(self):
    #     request = super(PurchaseRequisitionLine, self).compute_request()
    #     if self.requisition.operation_center:
    #         request.operation_center = self.requisition.operation_center.id
    #     if self.requisition.analytic_account:
    #         request.analytic_account = self.requisition.analytic_account.id
    #     return request


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'
    operation_center = fields.Many2One('company.operation_center', 'Operation Center')
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ])


class CreatePurchase(Wizard):
    'Create Purchase'
    __name__ = 'purchase.request.create_purchase'

    def _group_purchase_line_key(self, request):
        '''
        The key to group requests by lines
        A list of key-value as tuples of the purchase line
        '''
        return (
            ('product', request.product),
            ('description', request.description),
            ('unit', request.uom),
            ('operation_center', request.operation_center),
            ('analytic_account', request.analytic_account),
        )

    def transition_start(self):
        pool = Pool()
        Request = pool.get('purchase.request')
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Date = pool.get('ir.date')

        requests = self.records

        if (getattr(self.ask_party, 'party', None)
                and getattr(self.ask_party, 'company', None)):
            def compare_string(first, second):
                return (first or '') == (second or '')

            def to_write(request):
                return (not request.purchase_line
                    and not request.party
                    and request.product == self.ask_party.product
                    and compare_string(
                        request.description, self.ask_party.description))
            reqs = list(filter(to_write, requests))
            if reqs:
                Request.write(reqs, {
                        'party': self.ask_party.party.id,
                        })
            self.ask_party.product = None
            self.ask_party.description = None
            self.ask_party.party = None
            self.ask_party.company = None

        def to_ask_party(request):
            return not request.purchase_line and not request.party
        reqs = filter(to_ask_party, requests)
        if any(reqs):
            return 'ask_party'

        today = Date.today()

        requests = [r for r in requests if not r.purchase_line]

        keyfunc = partial(self._group_purchase_key, requests)
        requests = sorted(requests, key=sortable_values(keyfunc))

        purchases = []
        lines = []
        for key, grouped_requests in groupby(requests, key=keyfunc):
            grouped_requests = list(grouped_requests)
            try:
                purchase_date = min(r.purchase_date
                    for r in grouped_requests
                    if r.purchase_date)
            except ValueError:
                purchase_date = today
            if purchase_date < today:
                purchase_date = today
            purchase = Purchase(purchase_date=purchase_date)
            for f, v in key:
                setattr(purchase, f, v)
            purchases.append(purchase)
            for line_key, line_requests in groupby(
                    grouped_requests, key=self._group_purchase_line_key):
                line_requests = list(line_requests)
                line = self.compute_purchase_line(
                    line_key, line_requests, purchase)
                line.purchase = purchase
                line.requests = line_requests
                lines.append(line)
        Purchase.save(purchases)
        Line.save(lines)
        Request.set_purchased(requests)
        return 'end'

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        Line = pool.get('purchase.line')

        line = Line()
        line.unit_price = round_price(Decimal(0))
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.quantity for r in requests)
        line.purchase = purchase
        line.on_change_product()
        # Set again in case on_change's changed them
        for f, v in key:
            setattr(line, f, v)
        line.on_change_quantity()
        for req in requests:
            if req.analytic_account:
                line.analytic_accounts = [{
                    'root': req.analytic_account.root.id,
                    'account': req.analytic_account.id
                }]
                line.analytic_account = req.analytic_account.id
        return line


class PurchaseUpdateStart(metaclass=PoolMeta):
    'Purchase Update Start'
    __name__ = 'purchase.update.start'
    operation_center = fields.Many2One('company.operation_center', 'Operation Center')


class PurchaseUpdate(metaclass=PoolMeta):
    'Purchase Update'
    __name__ = 'purchase.update'

    def transition_accept(self):
        super(PurchaseUpdate, self).transition_accept()
        values = {}
        if self.start.operation_center:
            Purchase = Pool().get('purchase.purchase')
            Line = Pool().get('purchase.line')
            purchases = Purchase.browse(Transaction().context['active_ids'])
            purchases = [p for p in purchases if p.state == 'draft']
            values['operation_center'] = self.start.operation_center.id

            if values and purchases:
                lines = purchases[0].lines
                Line.write(list(lines), values)
        return 'end'
