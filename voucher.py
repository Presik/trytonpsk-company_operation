# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Voucher(metaclass=PoolMeta):
    'Voucher'
    __name__ = 'account.voucher'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center')

    def get_move_line(self, move, debit, credit):
        to_create = super(Voucher, self).get_move_line(move, debit, credit)
        if self.operation_center:
            to_create['operation_center'] = self.operation_center.id
        return to_create

    @fields.depends('lines', 'operation_center')
    def on_change_operation_center(self):
        if self.operation_center:
            for l in self.lines:
                if not l.operation_center:
                    l.operation_center = self.operation_center

    @classmethod
    def post(cls, vouchers):
        error_message = False
        for voucher in vouchers:
            if not voucher.operation_center:
                error_message = True
                break
            for l in voucher.lines:
                if not l.operation_center:
                    error_message = True
                    break
        if error_message:
            raise UserError(gettext('company_operation.msg_missing_operation_center'))
        super(Voucher, cls).post(vouchers)


class VoucherLine(metaclass=PoolMeta):
    'Voucher Line'
    __name__ = 'account.voucher.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center')

    def get_move_line(self, move_id):
        line = super(VoucherLine, self).get_move_line(move_id)
        line['operation_center'] = self.operation_center.id
        return line


class Note(metaclass=PoolMeta):
    'Note'
    __name__ = 'account.note'

    @classmethod
    def post(cls, records):
        for note in records:
            for line in note.lines:
                if not line.operation_center:
                    raise UserError(gettext('company_operation.msg_missing_operation_center'))
        super(Note, cls).post(records)

class NoteLine(metaclass=PoolMeta):
    'Note Line'
    __name__ = 'account.note.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center')

    def get_move_line(self):
        values = super(NoteLine, self).get_move_line()
        if self.operation_center:
            for value in values:
                value['operation_center'] = self.operation_center.id
        return values


class AddZeroAdjustmentStart(metaclass=PoolMeta):
    __name__ = 'account_voucher.add_zero_adjustment.start'
    operation_center = fields.Many2One('company.operation_center', 'Operation Center', required=True)
    analytic_account = fields.Many2One('analytic_account.account', 'Analytic Account')


class AddZeroAdjustment(metaclass=PoolMeta):
    __name__ = 'account_voucher.add_zero_adjustment'

    def get_line_value(self):
        line = super(AddZeroAdjustment, self).get_line_value()
        line['operation_center']= self.start.operation_center.id
        account = self.start.analytic_account
        line['analytic_accounts'] = [('create', [{'root': account.root, 'account': account.id}])]
        return line