from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.model import fields, ModelView

STATES = {
    'readonly': Eval('state') != 'draft',
        }

class Asset(metaclass=PoolMeta):
    __name__= 'account.asset'
    
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'readonly': Eval('state').in_(['confirmed', 'processed']),
        })
    
    def get_move(self, line):
        move = super(Asset, self).get_move(line)
        if move:
            for line in move.lines:
                line.operation_center = self.operation_center
        return move
    
    
    