# This file is part of a module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pyson import Eval
from trytond.pool import PoolMeta


class OpCenterResUser(ModelSQL):
    'Op. Center - Res User'
    __name__ = 'company.operation_center-res.user'
    _table = 'company_operation_center_res_user'
    operation_center = fields.Many2One('company.operation_center', 'OC',
        ondelete='CASCADE', select=True, required=True)
    user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
        required=True)


class User(metaclass=PoolMeta):
    __name__ = "res.user"
    operation_center = fields.Many2One('company.operation_center', 'OC',
        domain=[
            # ('id', 'in', Eval('operation_centers', [])),
        ])
    operation_centers = fields.Many2Many('company.operation_center-res.user', 'user',
        'operation_center', 'Op. Centers', domain=[])

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._preferences_fields.extend([
            'operation_center',
        ])
        cls._context_fields.insert(0, 'operation_center')
