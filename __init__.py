# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import operation_center
from . import invoice
from . import purchase
from . import sale
from . import sale_contract
from . import voucher
from . import move
from . import stock
from . import account
from . import account_reporting
from . import staff
from . import analytic_account
from . import asset
from . import user


def register():
    Pool.register(
        operation_center.OperationCenter,
        operation_center.OperationCenterUser,
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.PaymentsByOCStart,
        invoice.InvoicesStart,
        # invoice.PayInvoiceAsk,
        move.ReconcileLinesWriteOff,
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.PurchaseRequisition,
        purchase.PurchaseRequisitionLine,
        purchase.PurchaseRequest,
        sale_contract.ContractProductLine,
        sale_contract.SaleContractLine,
        sale.SaleLine,
        voucher.Voucher,
        voucher.VoucherLine,
        voucher.NoteLine,
        voucher.Note,
        move.Line,
        stock.Location,
        stock.ShipmentIn,
        stock.ShipmentInternal,
        purchase.PurchaseUpdateStart,
        account.AuxiliaryByOCStart,
        account_reporting.Context,
        account_reporting.OperationCenter,
        account_reporting.OperationCenterTimeseries,
        account_reporting.OperationCenterDetailedReport,
        analytic_account.Line,
        voucher.AddZeroAdjustmentStart,
        staff.Employee,
        staff.Payroll,
        staff.Liquidation,
        staff.PayrollLine,
        asset.Asset,
        user.OpCenterResUser,
        user.User,
        module='company_operation', type_='model')
    Pool.register(
        purchase.PurchaseUpdate,
        purchase.CreatePurchase,
        invoice.PaymentsByOC,
        invoice.Invoices,
        voucher.AddZeroAdjustment,
        account.AuxiliaryByOC,
        move.ReconcileLines,
        staff.PayrollGroup,
        module='company_operation', type_='wizard')
    Pool.register(
        invoice.PaymentsByOCReport,
        account.AuxiliaryByOCReport,
        invoice.InvoicesReport,
        account_reporting.OperationCenterReport,
        module='company_operation', type_='report')
