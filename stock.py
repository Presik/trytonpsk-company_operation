from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval
from trytond.transaction import Transaction


class Location(metaclass=PoolMeta):
    "Stock Location"
    __name__ = 'stock.location'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'invisible': Eval('type') != 'warehouse',
            }, depends=['purchase_state'])


class ShipmentIn(metaclass=PoolMeta):
    'Shipment In'
    __name__ = 'stock.shipment.in'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'required': Eval('state') == 'received',
        })

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        cls.reference.states = {
            'required': Eval('state').in_(['received']),
        }

    @classmethod
    def receive(cls, shipments):
        for shipment in shipments:
            context = Transaction().context
            context.update({
                'operation_center': shipment.operation_center.id if shipment.operation_center else None,
                'reference': shipment.reference,
                # 'analytic_account': shipment.analytic_account.id if shipment.analytic_account else None,
            }) 
            with Transaction().set_context(context):
                super(ShipmentIn, cls).receive([shipment])


class ShipmentInternal(metaclass=PoolMeta):
    'Shipment Internal'
    __name__ = 'stock.shipment.internal'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'required': Eval('state').in_(['wait', 'draft']),
        })

    approbal_date = fields.Date('Approbal Date', states={
            'readonly': True,
            }, depends=['state'],
        help="When the stock is approbal.")

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls.planned_date.states = {
            'required': True,
        }

    @classmethod
    def wait(cls, shipments, moves=None):
        super(ShipmentInternal, cls).wait(shipments)
        Date = Pool().get('ir.date')
        today = Date.today()
        cls.write([s for s in shipments if not s.approbal_date], {
                'approbal_date': today,
                })
