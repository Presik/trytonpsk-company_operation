# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

STATES = {
    'readonly': Eval('state') != 'draft',
}


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'readonly': Eval('sale_state') != 'draft',
        },
        depends=['sale_state'], required=True)

    def get_invoice_line(self):
        lines = super(SaleLine, self).get_invoice_line()
        if self.operation_center:
            for line in lines:
                line.operation_center = self.operation_center.id
        return lines
